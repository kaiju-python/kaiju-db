
*kaiju-db v. 2.1.12*

- feat: after update/create hooks for SQL service


###

*kaiju-db v. 2.1.12*

- feat: after update/create hooks for SQL service


###

*kaiju-db v. 2.1.11*

- feat: engine_settings arbitrary pg engine init set
- feat: fallback_hosts postgres option

- fix: change fallback_host setting format

###

*kaiju-db v. 2.1.10*



###

*kaiju-db v. 2.1.9*

- feat: echo param in DatabaseService to print queri
- feat: conn pool pre-ping to handle dead conns
- feat: added sql service with perm hook template

- fix: proper perm hook names in permhook mixin
- fix: fixed python version in ci
- fix: better PermHook arrangement of base classes
- fix: tweaked limits on get query

###

*kaiju-db v. 2.1.7*

- feat: echo param in DatabaseService to print queri
- feat: conn pool pre-ping to handle dead conns
- feat: added sql service with perm hook template

- fix: proper perm hook names in permhook mixin
- fix: fixed python version in ci
- fix: better PermHook arrangement of base classes
- fix: tweaked limits on get query

###
