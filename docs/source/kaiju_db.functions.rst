**functions** - SQL functions
=============================

.. autoclass:: kaiju_db.functions.SQLFunctionsRegistry
   :members:
   :exclude-members: get_key, items, keys, values
   :show-inheritance:
   :inherited-members:

.. autodata:: kaiju_db.functions.SQL_FUNCTIONS

.. autoclass:: kaiju_db.functions.UserFunction

.. autoclass:: kaiju_db.functions.DDL
