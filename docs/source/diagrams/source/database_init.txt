participant "SQLService" as srv
participant "DatabaseService" as db
database "DB" as pg

frame database init

group pre-init
  note over srv: Registering tables\nin global metadata.
  srv->db: DatabaseService.add_table
end

group init
	alt DatabaseService.init_db is True
    	note over db,pg: Init database using\nroot credentials.
    	db->pg: create database
        db->pg: create pg extensions
        db->pg: create user
        box over db: Drop pool and clear\nroot credentials
    end
    alt DatabaseService.init_tables is True
    	note over db,pg: Init functions from //functions_registry//,\ninit tables from the global metadata\nusing the user connection pool.
    	db->pg: create functions
        db->pg: create tables
    end
end

group post-init
	box over srv: Performing .init()\n SQL commands,\n migrations, etc.

end
