**services** - application services
===================================

.. autoclass:: kaiju_db.services.DatabaseService
   :members:
   :exclude-members: init, close, closed, discover_service, service_name
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_db.services.DatabaseMigrationService
   :members:
   :exclude-members: init, close, closed, discover_service, service_name
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_db.services.FixtureService
   :members:
   :exclude-members: init, close, closed, discover_service, service_name
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_db.services.SQLService
   :members:
   :exclude-members: ErrorCode, routes, permissions, validators, discover_service
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_db.services.PermHook
   :members:
   :exclude-members: routes, permissions, validators, service_name
   :show-inheritance:
   :inherited-members:
