**types** - columns data types
==============================

.. autoclass:: kaiju_db.types.CITEXT

.. autoclass:: kaiju_db.types.Ltree

.. autoclass:: kaiju_db.types.MigrationState
   :members:
