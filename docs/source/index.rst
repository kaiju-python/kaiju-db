.. include:: readme.rst

Guides
------

.. toctree::
   :maxdepth: 1

   user_guide
   dev_guide

Library Reference
-----------------

.. toctree::
   :maxdepth: 1

   kaiju_db.functions
   kaiju_db.services
   kaiju_db.types

.. include:: license.rst
