
.. image:: https://badge.fury.io/py/kaiju-db.svg
    :target: https://pypi.org/project/kaiju-db
    :alt: Latest package version

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style - Black

`Python <https://www.python.org>`_ >=3.11 `Postgres <https://www.postgresql.org>`_ >=14

Database connector services, database initialization, fixtures initialization, migrations.
Interfaces for remote interaction with tables via RPC.
See `documentation <https://kaiju-db.readthedocs.io/>`_ for more info.

Installation
------------

.. code-block:: console

  pip install kaiju-db

You need a `kaiju application <https://gitlab.com/kaiju-python/kaiju-base-app>`_ to run these services.
